Categories:Internet
License:GPL-3.0
Author Name:Niles Rogoff
Author Email:lain@rogoff.xyz
Web Site:https://dangeru.us
Source Code:https://github.com/nilesr/United4
Issue Tracker:https://github.com/nilesr/United4/issues

Auto Name:la/u/ncher
Summary:The official app for danger/u/, dangeru.us
Description:
The official app for danger/u/, [https://dangeru.us/ dangeru.us]

Based on the Sukeban Games visual novel Va-11 Hall-A. Assets used with
permission.
.

Repo Type:git
Repo:https://github.com/nilesr/United4

Build:4.1.3,413
    commit=8d89932
    subdir=app
    gradle=yes

Build:4.1.4,414
    commit=4.1.4
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:4.1.4
Current Version Code:414
